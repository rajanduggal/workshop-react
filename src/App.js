import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';

var data = require('./DATA_STUDENTS.json');

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sideNavExpanded: false,
      hover: false
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios.get(window.API_URL).then((res) => {
      let dat = this.filterByClass(res.data);
      this.setState({ students: dat });
    }).catch(err => {
      // debugger
    })
  }

  filterByClass(data) {
    let unique = [...new Set(data.map(p => p.class))];
    let final = []
    unique.forEach((el) => {
      let temp = [];
      data.forEach((da) => {
        if (el == da.class) {
          temp.push({ section: da.section, student: { name: da.name, age: da.age, sports: da.sports, gender: da.gender } });
        }
      })
      final.push(temp);
    })
    return final;
  }

  toggleSideNav(student, value) {
    this.setState({ selectedStudent: student, sideNavExpanded: value });
  }

  hoverAction(e, student, hover) {
    let top = e.clientY;
    let left = e.clientX;
    let hoveredStudent = student;
    this.setState({ top, left, hover, hoveredStudent });
  }

  render() {

    let { sideNavExpanded, students, selectedStudent, hover, top, left, hoveredStudent } = this.state;

    return (
      <div className="container">
        <div className="school-title">ThoughtWorks School</div>
        <div className="students-list-container">
          {data.class.map((el) => {
            return (
              <div key={el.rollNumber} className="student-item">
                <div className="class-item-name">Class: {el.name}</div>
                {el.sections.map((ele) => {
                  return (
                    <div className="students-cont">
                      <div>{ele.name}</div>
                      <div className="students-list">{ele.students.map((elel) => {
                        return (
                          <div>
                            <div className="student-item-name" onClick={() => { this.toggleSideNav(elel, true) }}>
                              {elel.name}
                              {<div className="hover-details">
                                <div>Name: {elel.name}</div>
                                <div>Age: {elel.age}</div>
                                <div>Gender: {elel.gender}</div>
                                <div>Sports: {elel.sports}</div>
                              </div>}
                            </div>
                          </div>
                        )
                      })}</div>
                    </div>
                  )
                })}
              </div>
            )
          })}
        </div>
        {sideNavExpanded && <div className="sidenav">
          <span className="close-sidenav" onClick={() => { this.toggleSideNav({}, false) }}>X</span>
          <div>Student Details</div>
          <div className="selected-student">
            <div>Name: <input type="text" value={selectedStudent.name} /></div>
            <div>Age: {selectedStudent.age}</div>
            <div>Gender: {selectedStudent.gender}</div>
            <div>Sports: <input type="text" value={selectedStudent.sports} /></div>
          </div>
        </div>}
      </div>
    )
  }
}

export default App;
