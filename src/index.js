import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

function initApp() {
    ReactDOM.render(<App />, document.getElementById('root'));
}

window.API_URL = 'https://student-management-api-1u3cd4j7s.now.sh/students';

initApp();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
